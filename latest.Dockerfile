FROM nginx:1.25.4-alpine

ENV TARGET=/usr/share/nginx/html

# Remove default NGINX HTML files
RUN rm -rf /usr/share/nginx/html/*

# Get all the archive static HTML and put it into place
# Include the versions found in 'content/versions.json' under "current" and "last_minor"
#
# Starting with 17.5, archive images have a new address:
# registry.gitlab.com/gitlab-org/gitlab-docs/archives:X.Y
COPY --from=registry.gitlab.com/gitlab-org/gitlab-docs/archives:17.8 ${TARGET} ${TARGET}
COPY --from=registry.gitlab.com/gitlab-org/gitlab-docs/archives:17.7 ${TARGET} ${TARGET}
COPY --from=registry.gitlab.com/gitlab-org/gitlab-docs/archives:17.6 ${TARGET} ${TARGET}
