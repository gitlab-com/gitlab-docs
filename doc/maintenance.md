# GitLab docs site maintenance

The Technical Writing team performs regular maintenance for [GitLab Docs](https://docs.gitlab.com) and the
`gitlab-docs` project.

## Deployment process

We use [GitLab Pages](https://about.gitlab.com/features/pages/) to build and
host this website.

The site is built and deployed automatically in GitLab CI/CD jobs.
See [`.gitlab-ci.yml`](../.gitlab-ci.yml)
for the current configuration. The project has [scheduled pipelines](https://docs.gitlab.com/ee/ci/pipelines/schedules.html)
that build and deploy the site every hour.

## Survey banner

In case there's a survey that needs to reach a big audience, the docs site has
the ability to host a banner for that purpose. When it is enabled, it's shown
at the top of every page of the docs site.

To publish a survey, edit [`banner.yaml`](/content/_data/banner.yaml) and:

1. Set `show_banner` to `true`.
1. Under `description`, add what information you want to appear in the banner.
   Markdown is supported.

To unpublish a survey, edit [`banner.yaml`](/content/_data/banner.yaml) and
set `show_banner` to `false`.

## CSP header

The GitLab docs site uses a [Content Security Policy (CSP) header](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP)
as an added layer of security that helps to detect and mitigate certain types of
attacks, including Cross Site Scripting (XSS) and data injection attacks.
Although it is a static site, and the potential for injection of scripts is very
low, there are customer concerns around not having this header applied.

It's enabled by default on <https://docs.gitlab.com>, but if you want to build the
site on your own and disable the inclusion of the CSP header, you can do so with
the `DISABLE_CSP` environment variable:

```shell
DISABLE_CSP=1 make compile
```

### Add or update domains in the CSP header

The CSP header and the allowed domains can be found in the [`csp.html`](../layouts/csp.html)
layout. Every time a new font or Javascript file is added, or maybe updated in
case of a versioned file, you need to update the `csp.html` layout, otherwise
it can cause the site to misbehave and be broken.

### No inline scripts allowed

To avoid allowing `'unsafe-line'` in the CSP, we cannot use any inline scripts.
For example, this is prohibited:

```html
<script>alert('Hello world');</script>
```

Instead, JavaScript should be extracted to its own files. See [Add a new bundle](development.md#add-a-new-bundle) for more information.

### Test the CSP header for conformity

To test that the CSP header works as expected, you can visit
<https://cspvalidator.org/> and paste the URL that you want tested.

## Review app tokens

The `gitlab-docs` project uses two access tokens for review apps:

- `DOCS_PROJECT_API_TOKEN` is a [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html).
  Other projects use this token to poll the review app deployment pipeline and determine if the pipeline has completed successfully.
- `DOCS_TRIGGER_TOKEN` is a [trigger token](https://docs.gitlab.com/ee/ci/triggers/index.html#create-a-pipeline-trigger-token).
  Other projects use this token to authenticate with the `gitlab-docs` project and trigger a review app deployment pipeline.

All projects with documentation review apps use two of these tokens when running the
[`trigger-build` script](https://gitlab.com/gitlab-org/gitlab/-/blob/master/scripts/trigger-build.rb)
to deploy a review app. Review apps stop working if you change or delete these tokens, or if the tokens expire.

### Regenerate `DOCS_PROJECT_API_TOKEN` in `gitlab-docs`

Regenerate the `DOCS_PROJECT_API_TOKEN` project access token when it expires or because of a security issue.
You can immediately revoke the existing token because it is used only for review apps or project maintenance.

Prerequisites:

- You must have at least the Maintainer role in the `gitlab-docs` project.

To regenerate `DOCS_PROJECT_API_TOKEN`:

1. In [`gitlab-docs`](https://gitlab.com/gitlab-org/gitlab-docs), go to **Settings > Access tokens**.
1. In **Active project access tokens**, find the entry for `DOCS_PROJECT_API_TOKEN` and select **Revoke** (**{remove}**).
1. Select **Add new token**, and fill in the following values:
   - **Token name**: `DOCS_PROJECT_API_TOKEN`.
   - **Expiration date**: Set to be 12 months into the future.
   - **Select a role**: `Developer`.
   - **Select scopes**: `api`.
1. Select **Create project access token**.
1. After the token is created, go to **Your new project access token** at the top
   and copy the token value. It should start with `glpat-`.

#### Update `DOCS_PROJECT_API_TOKEN` project access token in other projects

After regenerating the `DOCS_PROJECT_API_TOKEN` project access token in the `gitlab-docs` project, you must update the token in the projects that use it.

Prerequisites:

- You must have at least the Maintainer role in the project.

To update the `DOCS_PROJECT_API_TOKEN` project access token in projects that use it:

1. On the left sidebar, select **Search or go to** and find each of the following projects.

   - `gitlab`
   - `gitlab-runner`
   - `omnibus-gitlab`
   - `charts`
   - `gitlab-operator`

1. In each project, select **Settings > CI/CD**.
1. Expand **Variables**.
1. On the `DOCS_PROJECT_API_TOKEN` CI/CD variable, select **Edit** (**{pencil}**) and update the **Value** field with
   the new project access token. Don't change any other setting.

### Regenerate `DOCS_TRIGGER_TOKEN` in `gitlab-docs`

Regenerate the `DOCS_TRIGGER_TOKEN` pipeline trigger token when it expires or because of a security issue.
You can immediately revoke the existing token because it is used only for review apps or project maintenance.

Prerequisites:

- You must have at least the Maintainer role in the project.

To regenerate `DOCS_TRIGGER_TOKEN`:

1. In [`gitlab-docs`](https://gitlab.com/gitlab-org/gitlab-docs), go to **Settings > CI/CD** and expand **Pipeline trigger tokens**.
1. In **Active pipeline trigger tokens**, find the entry for `DOCS_TRIGGER_TOKEN` and select **Revoke trigger** (**{remove}**).
1. Select **Add new token**.
1. Under **Description**, fill in `DOCS_TRIGGER_TOKEN`.
1. Select **Create pipeline trigger token**.
1. After the token is created, copy the token value from the table.

#### Update project trigger token in other projects

After regenerating the `DOCS_TRIGGER_TOKEN` project trigger token in the `gitlab-docs` project, you must update the token in the projects that use it.

Prerequisites:

- You must have at least the Maintainer role in the project.

To update the `DOCS_TRIGGER_TOKEN` project trigger token in projects that use it:

1. On the left sidebar, select **Search or go to** and find each of the following projects.

   - `gitlab`
   - `gitlab-runner`
   - `omnibus-gitlab`
   - `charts`
   - `gitlab-operator`

1. In each project, select **Settings > CI/CD**.
1. Expand **Variables**.
1. On the `DOCS_TRIGGER_TOKEN` CI/CD variable, select **Edit** (**{pencil}**) and update the **Value** field with the
   new project access token. Don't change any other setting.

## Delete environments token

The `DELETE_ENVIRONMENTS_TOKEN` token is used by `gitlab-docs` to
[delete stale review app environments](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/21340eb6075dfaf65508d03b3177ed01a47a4187/.gitlab/ci/build-and-deploy.gitlab-ci.yml#L147).

### Regenerate and update `DELETE_ENVIRONMENTS_TOKEN`

To regenerate `DELETE_ENVIRONMENTS_TOKEN`, follow the steps for
[regenerating the `DOCS_PROJECT_API_TOKEN`](#regenerate-docs_project_api_token-in-gitlab-docs) token, except:

- Use `DELETE_ENVIRONMENTS_TOKEN` as the token name.
- Update the CI/CD variable in the `gitlab-docs` project only.

## Danger bot token

The `gitlab-docs` project has the `DANGER_GITLAB_API_TOKEN` project access token set, which is set as a CI/CD variable
for the <https://gitlab.com/gitlab-org/components/danger-review> component. This component adds comments to merge
requests based on Danger configuration.

If not set, Danger comments aren't added to merge requests.

### Regenerate `DANGER_GITLAB_API_TOKEN`

To regenerate `DANGER_GITLAB_API_TOKEN`:

1. In `gitlab-docs`, go to **Settings > Access Tokens**.
1. In **Active project access tokens**, find the entry for `DANGER_GITLAB_API_TOKEN` and
   select **Revoke**.
1. Select **Add new token**, and fill in the following values:
   - **Token name**: `DANGER_GITLAB_API_TOKEN`.
   - **Expiration date**: End of the year.
   - **Select a role**: `Developer`.
   - **Select scopes**: `api`.
   - **Description** : `DANGER_GITLAB_API_TOKEN`.
1. Select **Create project access token**.
1. After the token is created, go to **Your new project access token** at the top
   and copy the token value. It should start with `glpat-`.
1. In `gitlab-docs`, go to the [CI/CD variables settings](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project),
   expand **Variables**, select **Edit** for the `DANGER_GITLAB_API_TOKEN` CI/CD variable, and update the
   **Value** field with the new token.

## Redirects

The GitLab Docs site has two kinds of redirects.

If both of the redirects are present, the [HTML meta tag redirects](#html-meta-tag-redirects)
override the [Pages redirects](https://docs.gitlab.com/ee/user/project/pages/redirects.html#files-override-redirects).

### HTML meta tag redirects

If a page has the `redirect_to` metadata in the YAML front matter, it
redirects to the path defined in the metadata:

1. When Nanoc builds the site, it makes some changes in the [`preprocess`](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/4fc73c9a5f1652cc2e0b284bfe1e937887a37183/Rules#L6)
   step. For the redirects, it [checks for a `redirect_to` item identifier](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/4fc73c9a5f1652cc2e0b284bfe1e937887a37183/Rules#L21-28)
   (the YAML metadata) and changes the extension from `.md` to `.html`.
1. Next, it compiles all the markdown files into HTML. In this step, it first checks
   [if the `redirect_to` metadata is present](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/4fc73c9a5f1652cc2e0b284bfe1e937887a37183/Rules#L61),
   and if it is, the [redirect layout is used](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/4fc73c9a5f1652cc2e0b284bfe1e937887a37183/Rules#L105).
1. The [redirect layout](../layouts/redirect.html)
   is a simple HTML page that essentially does two things:
   - It uses an [`http-equiv` meta tag](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/4fc73c9a5f1652cc2e0b284bfe1e937887a37183/layouts/redirect.html#L8)
     with the URL set to the one defined in the `redirect_to` metadata of the
     markdown file:

     ```html
     <meta http-equiv="refresh" content="0; url=<%= @item[:redirect_to] %>">
     ```

   - If for some reason this doesn't work,
     [there's an additional URL](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/4fc73c9a5f1652cc2e0b284bfe1e937887a37183/layouts/redirect.html#L24-26)
     that explicitly points to the redirect URL that the user can manually click
     and get redirected:

     ```html
     <a href="<%= @item[:redirect_to] %>">Click here if you are not redirected.</a>
     ```

### Pages redirects

The GitLab Docs site is deployed in GitLab Pages and can leverage some of its
features. We can enable server-side redirects by using a
[`_redirects` file](https://docs.gitlab.com/ee/user/project/pages/redirects.html)
that contains the redirect rules.

We have a lot of redirects, so maintaining this plaintext file is cumbersome.
Instead, we use a YAML file which is then converted into the `_redirects` file:

1. The [`redirects.yaml` file](../content/_data/redirects.yaml)
   contains the redirect entries for the Docs site:

   | Entry          | Required               | Description |
   | -------------- | ---------------------- | ----------- |
   | `from`         | **{check-circle}** Yes | The old path of the page to redirect from. |
   | `to`           | **{check-circle}** Yes | The new path of the page to redirect to.   |
   | `remove_date`  | **{dotted-circle}** No | Not used by any code, shows when to remove entries from `redirects.yaml`. |

   These entries can be added manually, or by running the [clean up redirects Rake task](raketasks.md#clean-up-redirects) once a month.

1. Every time the site is built in the pipeline, the [redirects Rake task executes](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/4fc73c9a5f1652cc2e0b284bfe1e937887a37183/.gitlab-ci.yml#L194-195).
1. The [Rake task creates `public/_redirects`](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/4fc73c9a5f1652cc2e0b284bfe1e937887a37183/Rakefile#L217-231) by parsing `redirects.yaml`.
1. After Pages deploys the site, the `_redirects` file is at <https://docs.gitlab.com/_redirects>,
   and the redirects should be working.
